<?php

    require_once    'vendor/autoload.php';
    include         'autoload.php';

    $core = new Core();

    // Set handler for all Exceptions
    set_exception_handler ( 'exception_handler' );
    function exception_handler(Throwable $e){
        Functions::showException( $e );
    }

    $container = $core->buildDi();

    $core -> runApplication();

?>
