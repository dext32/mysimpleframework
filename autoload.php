<?php
//Define autoloader
spl_autoload_register('__autoload');

function __autoload($className) {

    if (file_exists($className . '.php')) {
        require_once $className . '.php';
        return true;
    }
    if (file_exists('src/'.$className . '.php')) {
        require_once 'src/'.$className . '.php';
        return true;
    }
    if (file_exists('src/Entity/'.$className . '.php')) {
        require_once 'src/Entity/'.$className . '.php';
        return true;
    }
    if (file_exists('src/Controllers/'.$className . '.php')) {
        require_once 'src/Controllers/'.$className . '.php';
        return true;
    }
    if (file_exists('src/Repositories/'.$className . '.php')) {
        require_once 'src/Repositories/'.$className . '.php';
        return true;
    }
    if (file_exists('src/Routes/'.$className . '.php')) {
        require_once 'src/Routes/'.$className . '.php';
        return true;
    }
    return false;
}