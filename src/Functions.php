<?php

class Functions{


    public static function Exception($message)
    {
        try {
            throw new Exception($message);
        }catch (Throwable $e)
        {
            Functions::showException($e);
            exit();
        }
    }


    public static function showException(Throwable $e)
    {
        if (Core::$config['system']['environment'] == 'production')
        {

            echo '<script type="text/javascript">document.body.innerHTML = \'\';</script>';

            echo '<div style="background-color: white; width:1160px; padding:100px; padding-bottom: 100px; margin-top:40px; margin-left: auto; margin-right: auto; text-align: center; font-size: 20px;">';
            echo 'We\'re sorry, something went wrong. <br />';
            echo 'Please contact with Administrator and give him this code ( '.$e->getCode().' )';
            echo '</div>';

            return;
        }

        echo '<script type="text/javascript">document.body.innerHTML = \'\';</script>';

        echo '<div style="background-color: white; width:1160px; padding-bottom: 300px; margin-top:40px; margin-left: auto; margin-right: auto;">';

        echo '<div style="width:1200px; margin-left: auto; margin-right: auto">';

        echo '<div style="width:1160px; padding:20px; background-color: #A11;">Exception</div>';
        echo '<div style="width:1000px; background-color: white; padding:30px;">';
        print_r($e->getMessage());
        echo '<br />';
        echo '<b>' . $e->getFile() . '</b> on line: <b>' . $e->getLine() . '</b>';
        echo '</div>';
        echo '<div style="width:1160px; padding:20px; margin-bottom:20px; background-color:#EEE;">';
        echo '<pre>';
        echo($e->getTraceAsString());
        echo '</pre> <br /><br />';

        $file= file($e->getFile());
        echo '<pre>';
        for ($i = $e->getLine()-10 - 1; $i <= $e->getLine() -1 -1 ; $i++)
        {
            if (isset($file[$i]))
                echo '   '.($i+1).'. '. $file[$i];
        }
        echo '-> '.($e->getLine()).'. '.$file[$e->getLine()-1];
        for ($i = $e->getLine(); $i <= $e->getLine() +10 -1 ; $i++)
        {
            if (isset($file[$i]))
                echo '   '.($i+1).'. '.$file[$i];
        }
        echo '</pre>';

        echo '</div>';

        echo '</div>';
        echo '</div>';

    }

    public static function dd($value)
    {
        echo '<script type="text/javascript">document.body.innerHTML = \'\';</script>';

        echo '<div style="padding:30px;"><pre>';
        print_r($value);
        echo '</pre></div>';

        die();
    }

}