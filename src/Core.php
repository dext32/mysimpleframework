<?php

class Core
{
    public static $config;

    private $di_container;

    private $configData = [
        0 => 'config/config.ini',
        1 => 'text/global.lang.php'
    ];

    public function __construct()
    {
        if (file_exists($this->configData[0]) && (filesize($this->configData[0]) !== 0)) {
            $file = parse_ini_file($this->configData[0], true);
            self::$config = $file;
        } else {
            return false;
        }

        header('Content-Type: text/html; charset=' . self::$config['system']['charset']);

        error_reporting(self::$config['system']['errorReporting']); // Added another error catching, now it hide only warnings

        if (phpversion() < self::$config['system']['PHPVersion']) {
            die('Your server must be in' . self::$config['system']['PHPVersion'] . ' PHP version or higher.');
        }

        return null;
    }

    public function buildDi(){
        $builder = new DI\ContainerBuilder();
        $this->di_container = $builder->build();
        return $builder->build();
    }

    public function runApplication(){
        // show error reporting
        error_reporting(E_ALL);

        // start php session
        session_start();
		
		$templatePath = 'template/';

        // Include header file
        require $templatePath.'header.php';

            // Create Route object and Add Route List
            $Route = new Routes();
            require 'Routes/RouteList.php';

            // Get page from address and put it into page variable
            $page = null;
            if (isset($_GET['p']))
                $page = $_GET['p'];

            // Run Controller
            $Route->RunController($page, $this->di_container);

        // Include footer file
        require $templatePath.'footer.php';
    }

}