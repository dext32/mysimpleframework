<?php
/**
 * Created by PhpStorm.
 * User: DOM
 * Date: 23.03.2019
 * Time: 16:57
 */


class PdoMysql{

    public static $config;

    public $pdo;

    private $configData = [
        0 => 'config/config.ini',
    ];

    public function __construct()
    {

        if (file_exists($this->configData[0]) && (filesize($this->configData[0]) !== 0)) {
            $file = parse_ini_file($this->configData[0], true);
            self::$config = $file;
        } else {
            return false;
        }

        $driver = self::$config['db']['driver'];
        $host = self::$config['db']['host'];
        $db   = self::$config['db']['db_name'];
        $user = self::$config['db']['user'];
        $pass = self::$config['db']['password'];
        $charset = 'utf8mb4';

        $dsn = "$driver:host=$host;dbname=$db;charset=$charset";
        $options = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        try {
            $pdo = new PDO($dsn, $user, $pass, $options);
            $this->pdo = $pdo;
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
    }

}