<?php
if(isset($_SERVER['HTTP_REFERER'])) {
    $back = '<p><a href="' . $_SERVER['HTTP_REFERER'] . '" class="back">Back</a></p>';
} else {
    $back = null;
}

$lang['ENG'][0] = 'Sample text #1';
$lang['ENG'][1] = 'Sample text #2';
$lang['ENG'][2] = 'Sample text #3';